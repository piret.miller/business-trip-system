<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

		<h1>Add New Employee</h1>
       <form:form method="post" action="save">  
      	<table >  
         <tr>  
          <td>Role_of_employee_id : </td> 
          <td><form:input path="role_of_employee_id"  /></td>
         </tr>  
         <tr>  
          <td>First_name : </td> 
          <td><form:input path="first_name"  /></td>
         </tr> 
         <tr>  
          <td>Last_name : </td> 
          <td><form:input path="last_name"  /></td>
         </tr> 
         <tr>  
          <td>Personal_code :</td>  
          <td><form:input path="personal_code" /></td>
         </tr> 
         <tr>  
          <td>Is_active :</td>  
          <td><form:input path="is_active" /></td>
         </tr> 
         <tr>  
          <td> </td>  
          <td><input type="submit" value="Save" /></td>  
         </tr>  
        </table>  
       </form:form>  
