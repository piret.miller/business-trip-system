    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>  

	<h1>Employees List</h1>
	<table border="2" width="70%" cellpadding="2">
	<tr><th>Id</th><th>Role_of_employee_id</th><th>First_name</th><th>Last_name</th><th>Personal_code</th><th>Is_active</th><th>Edit</th><th>Delete</th></tr>
    <c:forEach var="employee" items="${list}"> 
    <tr>
    <td>${employee.id}</td>
    <td>${employee.role_of_employee_id}</td>
     <td>${employee.first_name}</td>
    <td>${employee.last_name}</td>
    <td>${employee.personal_code}</td>
    <td>${employee.is_active}</td>
    <td><a href="editemployee/${employee.id}">Edit</a></td>
    <td><a href="deleteemployee/${employee.id}">Delete</a></td>
    </tr>
    </c:forEach>
    </table>
    <br/>
    <a href="employeeform">Add New Employee</a>