package com.hellokoding.account.model;
import java.util.Date;

public class Employee_business_trip {
	private int id; 
	private int employee_id;
	private int business_trip_id;
	private String description;
	private Date start_date;
	private Date end_date;
	private boolean is_confirmed;
	private int number_of_days;
	private int number_of_night;
	private double daily_allowance;
	private double transport_costs;
	private double accommodation_costs;
	private double other_costs;   
	private String comments;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEmployee_id() {
		return employee_id;
	}
	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}
	public int getBusiness_trip_id() {
		return business_trip_id;
	}
	public void setBusiness_trip_id(int business_trip_id) {
		this.business_trip_id = business_trip_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getStart_date() {
		return start_date;
	}
	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}
	public Date getEnd_date() {
		return end_date;
	}
	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}
	public boolean isIs_confirmed() {
		return is_confirmed;
	}
	public void setIs_confirmed(boolean is_confirmed) {
		this.is_confirmed = is_confirmed;
	}
	public int getNumber_of_days() {
		return number_of_days;
	}
	public void setNumber_of_days(int number_of_days) {
		this.number_of_days = number_of_days;
	}
	public int getNumber_of_night() {
		return number_of_night;
	}
	public void setNumber_of_night(int number_of_night) {
		this.number_of_night = number_of_night;
	}
	public double getDaily_allowance() {
		return daily_allowance;
	}
	public void setDaily_allowance(double daily_allowance) {
		this.daily_allowance = daily_allowance;
	}
	public double getTransport_costs() {
		return transport_costs;
	}
	public void setTransport_costs(double transport_costs) {
		this.transport_costs = transport_costs;
	}
	public double getAccommodation_costs() {
		return accommodation_costs;
	}
	public void setAccommodation_costs(double accommodation_costs) {
		this.accommodation_costs = accommodation_costs;
	}
	public double getOther_costs() {
		return other_costs;
	}
	public void setOther_costs(double other_costs) {
		this.other_costs = other_costs;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}

}
