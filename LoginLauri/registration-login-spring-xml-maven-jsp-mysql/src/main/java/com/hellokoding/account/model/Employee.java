package com.hellokoding.account.model;

public class Employee {
	private int id;  
	private int role_of_employee_id;
	private String first_name;  
	private String last_name;  
	private String personal_code;  
	private boolean is_active;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getRole_of_employee_id() {
		return role_of_employee_id;
	}
	public void setRole_of_employee_id(int role_of_employee_id) {
		this.role_of_employee_id = role_of_employee_id;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getPersonal_code() {
		return personal_code;
	}
	public void setPersonal_code(String personal_code) {
		this.personal_code = personal_code;
	}
	public boolean isIs_active() {
		return is_active;
	}
	public void setIs_active(boolean is_active) {
		this.is_active = is_active;
	}
	  

}
