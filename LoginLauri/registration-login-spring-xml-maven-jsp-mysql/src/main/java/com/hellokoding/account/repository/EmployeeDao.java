package com.hellokoding.account.repository;  
import java.sql.ResultSet;  
import java.sql.SQLException;  
import java.util.List;  
import org.springframework.jdbc.core.BeanPropertyRowMapper;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.RowMapper;

import com.hellokoding.account.model.Employee;  

  
public class EmployeeDao {  
JdbcTemplate template;  
	  
	public void setTemplate(JdbcTemplate template) {  
	    this.template = template;  
	}  
	public int save(Employee p){  
	    String sql="insert into employee(role_of_employee_id, first_name, last_name, personal_code, is_active) values("+p.getRole_of_employee_id()+",'"+p.getFirst_name()+"','"+p.getLast_name()+"','"+p.getPersonal_code()+"',"+p.isIs_active()+")";  
	    return template.update(sql);  
	}  
	public int update(Employee p){  
	    String sql="update employee set role_of_employee_id="+p.getRole_of_employee_id()+",first_name='"+p.getFirst_name()+"',last_name='"+p.getLast_name()+"', personal_code='"+p.getPersonal_code()+"',is_active="+p.isIs_active()+" where id="+p.getId()+"";  
	    return template.update(sql);  
	}  
	public int delete(int id){  
	    String sql="delete from employee where id="+id+"";  
	    return template.update(sql);  
	}  
	public Employee getEmployeeById(int id){  
	    String sql="select * from employee where id=?";  
	    return template.queryForObject(sql, new Object[]{id},new BeanPropertyRowMapper<Employee>(Employee.class));  
	}  
	public List<Employee> getEmployees(){  
	    return template.query("select * from employee",new RowMapper<Employee>(){  
	        public Employee mapRow(ResultSet rs, int row) throws SQLException {  
	            Employee e=new Employee();  
	            e.setId(rs.getInt(1));  
	            e.setFirst_name(rs.getString(2));
	            e.setLast_name(rs.getString(3));
	            e.setPersonal_code(rs.getString(4));  
	            e.setIs_active(rs.getBoolean(5));  
	            return e;  
	        }  
	    });  
	}  
}  