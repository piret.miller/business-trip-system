package com.hellokoding.account.model;

public class Business_trip {
	private int id;  
	private int country_id;
	private int transport_id;
	private String accommodation_description;   
	private String comments;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCountry_id() {
		return country_id;
	}
	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}
	public int getTransport_id() {
		return transport_id;
	}
	public void setTransport_id(int transport_id) {
		this.transport_id = transport_id;
	}
	public String getAccommodation_description() {
		return accommodation_description;
	}
	public void setAccommodation_description(String accommodation_description) {
		this.accommodation_description = accommodation_description;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}

}
